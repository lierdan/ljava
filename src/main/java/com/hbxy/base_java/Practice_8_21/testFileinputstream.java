package com.hbxy.base_java.Practice_8_21;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class testFileinputstream {
    public static void main(String[] args) throws IOException {
        File file=new File(".\\src\\main\\java\\com\\hbxy\\base_java\\Practice_8_21\\test.txt");
        FileInputStream fis=new FileInputStream(file);

        int len=0;
        while ((len=fis.read())!=-1){

            System.out.print((char) len);

        }

        fis.close();

    }
}
