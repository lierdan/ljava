package com.hbxy.base_java.Practice_8_21;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class testFileoutstream {
    public static void main(String[] args) throws IOException {
        File file=new File(".\\src\\main\\java\\com\\hbxy\\base_java\\Practice_8_21\\test.txt");
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        FileOutputStream fos=new FileOutputStream(file);

        String str="hello world";
        byte[] bytes=str.getBytes();

        fos.write(bytes);

    fos.close();
    }
}
