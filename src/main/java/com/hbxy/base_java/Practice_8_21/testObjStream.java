package com.hbxy.base_java.Practice_8_21;

import java.io.*;

public class testObjStream {
    public static void main(String[] args) throws IOException {
        File file = new File(".\\src\\main\\java\\com\\hbxy\\base_java\\Practice_8_21\\test.txt");

        Stu s = new Stu("lj", 18);
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(s);

        oos.flush();
        oos.close();
        fos.close();

        InputStream inputStream=new FileInputStream(file);
        int len = 0;
        byte[] bytes=new byte[1024];
        inputStream.read(bytes);
        String str=new String(bytes);
        System.out.println("str = " + str);

    }
}
