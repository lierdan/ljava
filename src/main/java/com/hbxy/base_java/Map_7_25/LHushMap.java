package com.hbxy.base_java.Map_7_25;

import java.util.*;

/*
    1 HashMap集合
    采用散列表的存储结构，
    所以这种映射也称为散列映射．
    键必须唯一，
    不可冲突，
    每个键对应一个值，
    如果后存入的数据和先存入的数据具有相同的键，
    那么后来的值将覆盖先前的值．

 */

/*
             1 public void clear()方法
            清空映射．
            2 public boolean containsKey(Object key)方法
            如果有数据使用了参数中的键，返回true，否则返回false．
            3 public boolean containsValues(Object value)方法
            如果有数据使用了参数中的值，返回true，否则返回false．
            4 public V get(Object key)方法
            返回键值对中使用key参数作为键的值．
            5 public boolean isEmpty()方法
            如果键值对中不存在任何数据，则返回true，否则返回false．
            6 public V remove(Object key)方法
            删除集合中以参数作为键的键值对．
            7 public int size()方法
            返回集合中键值对的数量．
 */

/*
    程序08行创建一个HashMap类型的对象map，
    当采用String泛型时，说明了map集合中的数据类型种类，
    注意键值必须是String类型，09到011行使用put()方法为集合赋值
    ，012行通过keySet()方法获取当前集合中的键的集合，
    并将该集合赋值给Collection集合类型对象coll
    ，Collection类型的对象通常有一个用途就是保存键或值的集合，
    016行通过get()方法获取当前map集合中的值，注意该方法的参数必须是集合中的键，
    那么输出时的值能够和键逐一对应．
 */




public class LHushMap {
    public static void main(String[] args) {
        Map<String,String> map=new HashMap<>();

        map.put("1","张三");
        map.put("4","李四");
        map.put("1","王二");
        map.put("3","宋五");
        map.put("2","王三");

        Collection<String> coll=map.keySet();
        Iterator<String> its=coll.iterator();
        while (its.hasNext()){
            String keyName=its.next();
            System.out.println(keyName+":"+map.get(keyName));
        }

        System.out.println(map.size());
        System.out.println(map.entrySet());
        System.out.println(map.toString());
        System.out.println(map.values());
        System.out.println(map.keySet());
        System.out.println(map.containsValue("宋五"));
        System.out.println(map.containsKey("3"));
        int i=1;


        while (true){
            String str=Integer.toString(i);
            System.out.println("str = " + str);
            if (map.containsKey(str)){
                i++;
                continue;
            }
            Scanner scanner=new Scanner(System.in);
                System.out.println("输入一个字符串 当输入\'时结束");
                String values=scanner.next();
                if (values.equals("\'")){
                    break;
                }
                map.put(str,values);
                i++;
        }
//        Collection<String> coll1=map.keySet();
//        Iterator<String> its1=coll1.iterator();
//        while (its1.hasNext()){
//            String keyName=its1.next();
//            System.out.println(keyName+":"+map.get(keyName));
//
//        }

        coll=map.keySet();
        its=coll.iterator();
        while (its.hasNext()){
            String keyName=its.next();
            System.out.println(keyName+":"+map.get(keyName));
        }

    }
}
