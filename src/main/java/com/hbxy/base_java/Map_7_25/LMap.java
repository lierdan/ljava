package com.hbxy.base_java.Map_7_25;
/*
      Map集合
        Map集合相对于前面的两种集合较为特殊，集合中的元素是成对出现的，称为“键值对（key--value）”．常见的方法如下：
            1 public void clear()方法
            清空映射．
            2 public boolean containsKey(Object key)方法
            如果有数据使用了参数中的键，返回true，否则返回false．
            3 public boolean containsValues(Object value)方法
            如果有数据使用了参数中的值，返回true，否则返回false．
            4 public V get(Object key)方法
            返回键值对中使用key参数作为键的值．
            5 public boolean isEmpty()方法
            如果键值对中不存在任何数据，则返回true，否则返回false．
            6 public V remove(Object key)方法
            删除集合中以参数作为键的键值对．
            7 public int size()方法
            返回集合中键值对的数量．
            常用的Map集合包括HashMap集合和TreeMap集合两种．

 */
public class LMap {
}
