package com.hbxy.base_java.Map_7_25;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class test {
    public static void main(String[] args) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("001", "张三");
        map.put("002", "李四");
        map.put("005", "王五");
        Collection<String> coll = map.keySet();
        Iterator<String> it = coll.iterator();
        while (it.hasNext()) {
            String keyName = it.next();
            System.out.println(keyName + ":" + map.get(keyName));
        }
    }


}




