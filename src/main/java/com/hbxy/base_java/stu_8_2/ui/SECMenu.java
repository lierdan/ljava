package com.hbxy.base_java.stu_8_2.ui;

import com.hbxy.base_java.stu_8_2.entity.Leader;
import com.hbxy.base_java.stu_8_2.entity.Monitor;
import com.hbxy.base_java.stu_8_2.entity.Student;

import java.util.Scanner;

public class SECMenu {

    int infoFlag;

    public SECMenu(int i) {
        this.infoFlag = i;
    }

    public SECMenu() {

    }


    public void secMenu() {
        System.out.println("----------------------------");
        System.out.println("        1.查看个人信息");
        System.out.println("        2.查看班级信息");
        System.out.println("        3.补全个人信息");
        System.out.println("        4.返回上一菜单");
        System.out.println("----------------------------");
    }


    public void petInfo(Student[] stus) {//完善信息
        Scanner in = new Scanner(System.in);
        System.out.println("请输入地址");
        String str_address = in.next();
        System.out.println("请输入手机号");
        String str_phonenum = in.next();
        stus[infoFlag].setAddress(str_address);
        stus[infoFlag].setPhoneNum(str_phonenum);
        System.out.println("填写完成");
    }


    public void perinfo(Student[] stu) {//查看个人信息
        if (stu[infoFlag] instanceof Student) {
            stu[infoFlag].PLSinfo();
        }
        if (stu[infoFlag] instanceof Monitor) {
            ((Monitor) stu[infoFlag]).PLMinfo();
        }
        if (stu[infoFlag] instanceof Leader) {
            ((Leader) stu[infoFlag]).PLLinfo();
        }
    }

    public void clsInfo(Student[] students) {

        String str_mname = "";//班长姓名
        String str_lname = "";//组长姓名；
        String str_sname = "";//学生姓名


        for (int i = 0; i < students.length; i++) {
            if (students[i] == null)
                continue;

            if (students[i] instanceof Monitor) {
                str_mname += students[i].getName() + "\t";
            }
            if (students[i] instanceof Leader) {
                str_lname += students[i].getName() + "\t";
            }
            if (students[i] instanceof Student) {
                str_sname += students[i].getName() + "\t";
            }


        }
        System.out.println("该班的班长为:" + str_mname);
        System.out.println("该班的组长有:" + str_lname);
        System.out.println("该班的学生有" + str_sname);

    }

    public void done(FirstMenu firstMenu, SECMenu secMenu, Student[] stus) {
        Scanner in = new Scanner(System.in);
        int flag = 0;//菜单选择

        do {
            secMenu.secMenu();//菜单
            flag = in.nextInt();
            switch (flag) {
                case 1:
                    secMenu.perinfo(stus);
                    break;
                case 2:
                    secMenu.clsInfo(stus);
                    break;
                case 3:
                    secMenu.petInfo(stus);
                    break;
                case 4:
                    break;
                default:
                    System.out.println("请输入正确数字");
            }
        } while (flag != 4);
    }


}
