package com.hbxy.base_java.stu_8_2.ui;

import com.hbxy.base_java.stu_8_2.entity.Leader;
import com.hbxy.base_java.stu_8_2.entity.Monitor;
import com.hbxy.base_java.stu_8_2.entity.Student;

import java.util.Scanner;

public class THIMenu {//3菜单

    public void thiMenu() {
        System.out.println("----------------------------");
        System.out.println("        1.查看学生组织");
        System.out.println("        2.设置班长");
        System.out.println("        3.设置组长");
        System.out.println("        4.返回上一菜单");
        System.out.println("----------------------------");
    }

    public void setMonitor(Student[] stus) {
        int length = 0;
        for (int i = 0; i < stus.length; i++) {

            if (stus[i] == null)
                continue;
            System.out.print(i + "、");
            System.out.println("姓名:" + stus[i].getName());
            length++;
        }
        Scanner in = new Scanner(System.in);
        int flag = 0;
        System.out.println("请输入设置学生的序号");
        while (true) {
            flag = in.nextInt();
            if (flag >= 0 && flag < length) {
                break;
            }
            System.out.println("请重新输入");
        }
        System.out.println();
//        stus[flag].setAddress("123456");
        Monitor monitor = new Monitor();
        monitor.setName(stus[flag].getName());
        monitor.setNum(stus[flag].getNum());
        monitor.setAddress(stus[flag].getAddress());
        monitor.setPhoneNum(stus[flag].getPhoneNum());
        stus[flag] = monitor;
        System.out.println("已设置" + stus[flag].getName() + "为班长");
    }

    public void setLeader(Student[] stus) {
        int length = 0;
        for (int i = 0; i < stus.length; i++) {

            if (stus[i] == null)
                continue;
            System.out.print(i + "、");
            System.out.println("姓名:" + stus[i].getName());
            length++;
        }
        Scanner in = new Scanner(System.in);
        int flag = 0;
        System.out.println("请输入设置学生的序号");
        while (true) {
            flag = in.nextInt();
            if (flag >= 0 && flag < length) {
                break;
            }
            System.out.println("请重新输入");
        }
        System.out.println();
//        stus[flag].setAddress("123456");
        Leader leader = new Leader();
        leader.setName(stus[flag].getName());
        leader.setNum(stus[flag].getNum());
        leader.setAddress(stus[flag].getAddress());
        leader.setPhoneNum(stus[flag].getPhoneNum());
        stus[flag] = leader;
        System.out.println("已设置" + stus[flag].getName() + "为组长");
    }


    public void done(FirstMenu firstMenu, THIMenu thiMenu, Student[] stus) {
        Scanner in = new Scanner(System.in);
        int flag = 0;//菜单选择

        do {
            thiMenu.thiMenu();
            flag = in.nextInt();
            switch (flag) {
                case 1:
                    new SECMenu().clsInfo(stus);
                    break;
                case 2:
                    setMonitor(stus);
                    break;
                case 3:
                    setLeader(stus);
                    break;
                case 4:
                    firstMenu.stMenu();
                    break;
                default:
                    System.out.println("请输入正确数字");
            }
        } while (flag != 4);
    }


}
