package com.hbxy.base_java.stu_8_2.ui;


import com.hbxy.base_java.stu_8_2.entity.Student;

import java.util.Scanner;

public class FirstMenu {
//    static final String str = "123";//常量的表达模式

    public void stMenu() {//开始菜单
        System.out.println("----------------------------");
        System.out.println("        学生管理系统");
        System.out.println("----------------------------");
        System.out.println("        1.注册");
        System.out.println("        2.登陆");
        System.out.println("        3.学生管理设置");
        System.out.println("        4.退出");
        System.out.println("----------------------------");
    }

    public int register(Student stu) {//添加信息,以单对象为参数
        Scanner in = new Scanner(System.in);
        System.out.println("请输入姓名");
        String str = in.next();
        stu.setName(str);
        System.out.println("信息录入成功");
        System.out.println("请输入学号");
        str = in.next();
        stu.setNum(str);
        System.out.println("信息录入成功");
        System.out.println("成功注册");
        return 1;
    }

    public Boolean addChoose(Student a[]) {//判断数组中对象是否null
        for (int i = 0; i < a.length; i++) {
            if (a[i] == null) {
                a[i] = new Student();
                if (register(a[i]) == 1)//添加信息
                    return true;
            } else
                continue;
        }
        return false;
    }

    public int findIndex(Student a[], String name) {//判断数组中对象是否null
        for (int i = 0; i < a.length; i++) {
            if (a[i] == null)
                continue;
            if (name.equals(a[i].getName()))//数据匹配返回下标
                return i;
        }
        return -1;//未找到
    }

    public int login(Student[] stu) {//登陆

        Scanner in = new Scanner(System.in);
        System.out.println("登陆");
        System.out.println("请输入用户名");
        String str_name = in.next();

        int flag = findIndex(stu, str_name);//查找相应对象数据，找到后返回对象所属对象数组下标，未找到返回-1
        if (flag == -1) {
            System.out.println("没有该用户");
            return flag;
        }

        System.out.println("请输入密码");
        String str_pas = in.next();
        System.out.println("用户名:" + str_name);
        System.out.println("密码: " + str_pas);
        if (str_name.equals(stu[flag].getName()) && str_pas.equals(stu[flag].getNum())) {
            System.out.println("登陆成功");
        } else {
            System.out.println("登录失败");
            flag = -1;
        }

        return flag;
    }


    public void done(FirstMenu firstMenu, SECMenu secMenu, THIMenu thiMenu,Student[] stus) {
        Scanner in = new Scanner(System.in);
        int flag = 0;//菜单选择
        int count = 0;//已有数据对象个数

        firstMenu.stMenu();//菜单

        while (flag != 4) {

            if (count == stus.length) {
                System.out.println("数组容量已满");
                break;
            }

            flag = in.nextInt();
            switch (flag) {
                case 1:
                    if (firstMenu.addChoose(stus)) {//注册
                        count++;
                    }
                    firstMenu.stMenu();//菜单
                    break;
                case 2:
                    int temp=0;
                   temp = firstMenu.login(stus);
                    if (temp != -1) {
                        secMenu = new SECMenu(temp);
                        secMenu.done(firstMenu,secMenu,stus);
                        firstMenu.stMenu();


                    } else
                        firstMenu.stMenu();
                    //登陆
                    break;
                case 3:
                    System.out.println("学生组织设置");
                    thiMenu.done(firstMenu,thiMenu,stus);//菜单
                    break;
                case 4:
                    System.out.println("退出");
                    break;
                default:
                    System.out.println("请输入正确数字");
            }
        }


    }


}
