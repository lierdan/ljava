package com.hbxy.base_java.stu_8_2;

import com.hbxy.base_java.stu_8_2.entity.Monitor;
import com.hbxy.base_java.stu_8_2.entity.Student;

public class test {
    public static void main(String[] args) {
        Student stu = new Student();
        Student stu1 = new Student();
        Student stu2 = new Student();

        Monitor monitor=new Monitor();
        stu1.setName("我是班长");
        stu2.setName("学生2");
        stu.setName("学生1");

        stu2=monitor;
        stu1.setName("我是班长");

        ((Monitor)stu2).work();
        ((Monitor)stu2).addBranch("15","16");
        ((Monitor)stu2).PLBranch();
    }
}
