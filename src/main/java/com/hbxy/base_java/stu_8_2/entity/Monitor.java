package com.hbxy.base_java.stu_8_2.entity;


public class Monitor extends Student {//班长

    {
        super.setPost("班长");
    }


    private String[] branch;

    public String[] getBranch() {
        return branch;
    }

    public void setBranch(String[] branch) {
        this.branch = branch;
    }


    public boolean addBranch(String... names) {

        setBranch(names);
        return true;
    }

    public void work() {
        System.out.println("工作属性:班长的工作为处理班级事务，管理组长");
    }

    public void PLBranch() {
        if (getBranch()!=null){
            String[] str = getBranch();
            System.out.println("下属组长为");


            for (int i = 0; i < str.length; i++) {
                System.out.print("\t " + str[i]);
            }

            System.out.println();
        }else System.out.println("暂时没有分配下属");

    }

    public void PLMinfo(){//显示信息

        toString();
        work();
        PLBranch();
    }

}
