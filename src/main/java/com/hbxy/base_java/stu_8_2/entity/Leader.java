package com.hbxy.base_java.stu_8_2.entity;

public class Leader extends Student{

    {
        super.setPost("组长");

    }    private String[] branch;

    public String[] getBranch() {
        return branch;
    }

    public void setBranch(String[] branch) {
        this.branch = branch;
    }


    public boolean addBranch(String... names) {

        setBranch(names);
        return true;
    }

    public void work() {
        System.out.println("工作属性:组长的工作为辅助班长，辅助组员");
    }

    public void PLBranch() {
        if (getBranch()!=null){
            String[] str = getBranch();
            System.out.println("下属人员为");


            for (int i = 0; i < str.length; i++) {
                System.out.print("\t " + str[i]);
            }

            System.out.println();
        }else System.out.println("暂时没有分配下属");

    }

    public void PLLinfo(){//显示信息

        toString();
        work();
        PLBranch();
    }

}
