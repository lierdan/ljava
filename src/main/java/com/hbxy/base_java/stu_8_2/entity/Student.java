package com.hbxy.base_java.stu_8_2.entity;

public class Student {//学生
    private String name;//学生姓名
    private String num;//学号
    private String address;//家庭住址
    private String phoneNum;//手机号
    private String post;//职务

    {//初始化对象

        address="未填写";
        post="无";
        phoneNum="未填写";

    }



    public String PLSinfo() {//显示学生个人信息

        System.out.println(
                "姓名：" + name + '\'' +
                ", 学号：" + num + '\'' +
                ", 地址：'" + address + '\'' +
                ", 手机号：'" + phoneNum + '\'' +
                ", 职务：'" + post + '\'' );

        return "姓名：" + name + '\'' +
                ", 学号：" + num + '\'' +
                ", 地址：'" + address + '\'' +
                ", 手机号：'" + phoneNum + '\'' +
                ", 职务：'" + post + '\'';
    }



    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }





    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }
}
