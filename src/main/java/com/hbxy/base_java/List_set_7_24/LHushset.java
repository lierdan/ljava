package com.hbxy.base_java.List_set_7_24;

import java.util.HashSet;
import java.util.Set;
/*
1 HashSet集合
    ******由哈希表构成，不能保证调用元素时的顺序和存入集合时的顺序一致，可以运行有空值存在．
 */
public class LHushset {

    public static void main(String[] args) {
        Set<String> set=new HashSet<String>();
        set.add("hello");
        set.add(":");
        set.add("world");
        new util().DoneIterator(set);

        Set<String> set1=new HashSet<String>();
        set1.add("hello");
        set1.add("js");
        new util().DoneIterator(set1);

        set1.addAll(set1);
        new util().DoneIterator(set);

        set.remove(":");
        new util().DoneIterator(set);


        System.out .println(set.size());
    }
}
