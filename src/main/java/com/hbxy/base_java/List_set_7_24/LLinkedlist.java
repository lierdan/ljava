package com.hbxy.base_java.List_set_7_24;

import com.hbxy.base_java._______;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
/*
    采用链表形式进行存储，内存中的元素之间可以不直接紧邻，
    但是存在逻辑上的紧邻关系，这种集合的优点是插入删除元素时很快，
    因为操作时不需要考虑后面的元素移动的问题．缺点是查找元素不方便，
    因为元素不是紧邻的，所以每个元素都要单独查找．
    LinkedList常用的方法如下：
    1 addFirst(E ojb)方法 (无)
    将指定对象插入链表的开头
    2 addLast(E ojb)方法
    将指定对象添加到链表的结尾
    3 getFirst()方法
    获取链表的开头对象
    4 getLast()方法
    获取链表的结尾对象
    5 removeFirst()方法
    去除开头元素
    6 removeLast()方法
    去除结尾元素
*/

public class LLinkedlist {

    public void DoneIterator(List list){
        Iterator its=list.iterator();
        while (its.hasNext()){
            System.out.println(its.next().toString());
        }
//        new _______();
    }
    public static void main(String[] args) {
        List<String> list=new LinkedList<String>();
        list.add("+");
        list.set(0,"hello");
        list.add(":");
        list.add("world");
        Iterator its=list.iterator();
        while (its.hasNext()){
            System.out.println(its.next());
        }
//        new _______();

        List<String>list1=new ArrayList<String>();
        list1.add("hello");
        list1.add("arraylist");

        its=list1.iterator();
        while (its.hasNext()){
            System.out.println(its.next());
        }
//        new _______();

        list.addAll(list1);
        new LLinkedlist().DoneIterator(list);

        list.remove(":");
        new LLinkedlist().DoneIterator(list);

//        list.clear();
//        //清空list集合
//        new LLinkedlist().DoneIterator(list);

        list.containsAll(list1);
        /*
        containsAll方法用于判断列表中是否包含指定collection的所有元素。
        语法  boolean containsAll(Collection<?> c)
        返回值：true或false，如果列表中包含指定collection对象的所有元素，则返回true；否则返回false。
         */
        new LLinkedlist().DoneIterator(list);
    }

}
