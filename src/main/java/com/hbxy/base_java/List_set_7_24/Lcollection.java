package com.hbxy.base_java.List_set_7_24;

import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Lcollection {
    /**
     *Collection是一种应用非常广泛的集合，包括Collection接口和多种实现类．
     * 该接口是List接口和Set接口的父接口，其中的方法一般不会直接被使用，
     * 而是通过List和Set来实现，所以Collection接口所定义的方法，
     * 有很多在List和Set中都是兼容的
     * ．Collection接口中常见的方法有如下几种：
     *
     */

    public static void main(String[] args) {
        java.util.Collection List =new ArrayList();
        /**
         * add方法
         */

        List.add("hbxy");
        List.add("jsjx");

        java.util.Collection List2=new ArrayList();
        /*
        2 addAll()方法
        用于将某个集合中的全部内容添加到另一个集合中．
         */
        List2.addAll(List);

        /*
        iterator()方法:集合中的迭代器，当我们在集合中添加了某些元素后，
        这些元素可以按照一定的顺序自动排序，用户使用这些元素时，就需要通过迭代器逐一取出．
         */
        Iterator it=List.iterator();
        while (it.hasNext()){
            System.out.println(it.next().toString());
        }

        /*
        4 removeAll()方法
        将集合中的对象全部移除，返回一个布尔类型的值．
        个人理解：removeAll(List list)
         */
        System.out.println("_________________________________________");
        System.out.println(List.removeAll(List2));
        List.add("c#");
        Iterator its=List.iterator();
        System.out.println("List");
        while (its.hasNext()){
            System.out.println(its.next().toString());
        }
        System.out.println("List2");
        Iterator its2=List2.iterator();
        while (its2.hasNext()){
            System.out.println(its2.next().toString());
        }
    }
}
