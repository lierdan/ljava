package com.hbxy.base_java.List_set_7_24;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class util {

    public void br(){
        /*
        庄严的分割线
         */
            System.out.println();
            System.out.println("__________________________________________");

    }

    public void DoneIterator(List list){
        /*
        迭代器显示
         */
        Iterator its=list.iterator();
        while (its.hasNext()){
            System.out.println(its.next().toString());
        }
        br();
    }

    public void DoneIterator(Set set){
        Iterator its=set.iterator();
        while (its.hasNext()){
            System.out.println(its.next().toString());
        }
        br();
    }

    public void DoneIterator(HashSet set){
        Iterator its=set.iterator();
        while (its.hasNext()){
            System.out.println(its.next().toString());
        }
        br();
    }



}
