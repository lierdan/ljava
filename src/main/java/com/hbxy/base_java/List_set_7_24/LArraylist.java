package com.hbxy.base_java.List_set_7_24;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LArraylist {
    /*
    List集合包含List接口和该接口的所有实现类，该接口继承于Collection接口，因此具有父接口的全部方法．
    List集合为列表类型，以线性方式存储数据，其中的数据可以重复出现．List集合主要包含以下几种方法：
    1 add(int index, Object ojb)方法
        向index索引位置添加一个对象，后面的对象分别后移一位，索引从0位置开始．
    2 addAll(Collection coll)方法
        向一个集合中添加另一个集合中的元素．
    3 remove(int index)方法
        清除集合中指定位置的对象．
    4 set(int index, Object obj)方法
        将集合中某个位置的对象替换为另一个对象．
    5 get(int index)方法
        获取集合中指定位置的对象．
    6 indexOf(Object obj)方法
        获取指定对象的索引位置，当存在多个对象时，返回第一个位置，没有对象时返回-1．
    7 lastIndexOf(Object obj)方法
        获得指定对象的索引位置，当存在多个对象时，返回最后一个位置，没有对象时返回-1．
    List接口的实现类有ArrayList和LinkedList两类．
     */
    public static void main(String[] args) {
        /*
        1 ArrayList类
            采用数组结构保存对象，
            也就是说在内存中，数据的分布是紧邻的．
            它的优点是：
                存储速度快，当确定其中某个元素的位置后，
                其余元素可以直接顺次获取．它的缺点是：插入和删除元素的效率较低，
                因为当插入或删除元素时，后面的元素必须顺次移动位置，这个时间开销是非常巨大的．
         */
        List <String>list=new ArrayList<String>();
        list.add("hello");
        list.add("world");
        list.add(1,":");
        Iterator its;
        its=list.iterator();
        while (its.hasNext()){
            System.out.print(its.next());
        }

        System.out.println();
        System.out.println("__________________________________________");
        System.out.println("__________________________________________");
        System.out.println(list.get(1));
        list.set(1,"+");
        its=list.iterator();
        while (its.hasNext()){
            System.out.print(its.next());
        }


        System.out.println();
        System.out.println("__________________________________________");
        System.out.println("__________________________________________");
        List<String> list1=new ArrayList<String>();
        list1.add("hello");
        list1.add("c");
//        list.addAll(list1);
        list.removeAll(list1);
        its=list.iterator();
        while (its.hasNext()){
            System.out.print(its.next());
        }

        System.out.println();
        System.out.println("__________________________________________");
        System.out.println("__________________________________________");
        System.out.println(list.lastIndexOf("+"));
        System.out.println(list.indexOf("world"));
    }
}
