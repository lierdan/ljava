package com.hbxy.base_java.List_set_7_24;

import java.util.Set;

public class Lset {
    /*
        存储的内容不是按照进入集合的编号顺序遍历，
        而是按照内存引用顺序进行，所以Set集合不能存在重复的元素值．
        Set集合包含Set接口和若干实现类，因为继承于Collection接口，
        所以Set接口的实现类可以具备Collection接口的功能．
     */
    public static void main(String[] args) {
        /*
        详情看Hushset、TreeSet
         */
    }
}
