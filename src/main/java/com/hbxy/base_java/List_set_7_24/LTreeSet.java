package com.hbxy.base_java.List_set_7_24;

import java.util.TreeSet;

/*
    2 TreeSet集合
        该集合又称为“树集合”，所表现出来的元素顺序不是按照添加顺序，而是按照数据的大小排列，层次间从上到下，从左到右依次排序．
    常用方法
        1 public boolean add(E obj)方法
        向集合添加节点，节点位置由参数比较后决定，添加成功返回true，失败返回false．
        2 public void clear()
        删除集合中的全部节点
        3 public void contains(Object obj)方法
        如果集合中包含参数指定的对象，返回true，否则返回false．
        4 public E first()方法
        返回集合中数值最小的数据，即开始数据．
        5 public E last()方法
        返回集合中数值最大的数据，即最后数据．
        6 public boolean isEmpty()方法
        判断集合是否为空，如果是，返回true，否则返回false．
        7 public int size()方法
        返回集合中节点的数量．
 */
public class LTreeSet {
    public static void main(String[] args) {
        TreeSet set=new TreeSet();
        System.out.println(set.isEmpty());
        new util().br();

        set.add(5);
        set.add(7);
        set.add(1);
        set.add(3);
        new util().DoneIterator(set);

        set.clear();
        new util().br();

        /*
        字符排序？首字母排序?
         */
        set.add("o");
        set.add("b");
        set.add("ca");
        set.add("zx");
        set.add("ab");
        new util().DoneIterator(set);

        System.out.println(set.size());
        new util().br();

        System.out.println(set.contains("g"));
        System.out.println(set.first());
        System.out.println(set.last());
    }
}
